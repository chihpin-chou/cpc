﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Microsoft.Extensions.Http;
using System.Net.Http;
using System.Threading.Tasks;

namespace CPC.Integration.Incoming
{
    
    public class StationInformation
    {        
        public HttpClient Client { get; }
        public HttpRequestMessage Message { get; }

        public StationInformation(HttpClient client, HttpRequestMessage message)
        {            
            Client = client;
            Message = message;
        }

        public async Task<string> GetPosts()
        {
            Client.Timeout = new TimeSpan(0, 0, 60);
            var response = Client.SendAsync(Message);            
            //response.EnsureSuccessStatusCode();
            
            var result = await response.Result.Content.ReadAsStringAsync();
            return result;
        }
    }
}