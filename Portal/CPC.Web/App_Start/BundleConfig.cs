﻿using System.Web;
using System.Web.Optimization;

namespace CPC.Web
{
    public class BundleConfig
    {
        // 如需統合的詳細資訊，請瀏覽 https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // 使用開發版本的 Modernizr 進行開發並學習。然後，當您
            // 準備好可進行生產時，請使用 https://modernizr.com 的建置工具，只挑選您需要的測試。
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-theme.css",
                      "~/Content/site.css",
                      "~/Content/my.css",
                      "~/Content/breadcrumb.css",
                      "~/Content/ribbon.css",
                      "~/Content/checkbox.css",
                      "~/Content/menu.css"));

            bundles.Add(new ScriptBundle("~/bundles/amcharts4").Include(
                        "~/Scripts/amcharts4/core.js",
                        "~/Scripts/amcharts4/charts.js",
                        "~/Scripts/amcharts4/themes/animated.js",
                        "~/Scripts/amcharts4/themes/material.js",
                        "~/Scripts/amcharts4/themes/kelly.js",
                        "~/Scripts/amcharts4/lang/zh_Hans.js"                        
                        ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-bundle").Include(
                     "~/Scripts/bootstrap.bundle.js"));

            bundles.Add(new ScriptBundle("~/bundles/admin").Include(
                     "~/Scripts/admin.js"));

            bundles.Add(new ScriptBundle("~/bundles/menu").Include(
                     "~/Scripts/menu.js"));
        }
    }
}
