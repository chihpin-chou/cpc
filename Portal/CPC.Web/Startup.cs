﻿using Microsoft.Owin;
using Owin;
using Microsoft.Extensions.Http;
using CPC.Integration.Incoming;
using Microsoft.Extensions.DependencyInjection;
using System;

[assembly: OwinStartupAttribute(typeof(CPC.Web.Startup))]
namespace CPC.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpClient<StationInformation>(c =>
            {
                c.BaseAddress = new Uri("https://blog.yowko.com");
            });
        }
    }
}
