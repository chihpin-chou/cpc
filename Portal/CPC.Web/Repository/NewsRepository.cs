﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using NLog;
using AutoMapper;
using CPC.DataModel;
using CPC.Web.Repository.Interface;

using System.Data.SqlClient;
using System.Configuration;

namespace CPC.Web.Repository
{
    public class NewsRepository : INewsRepository
    {
        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        //private MyCPCPortalEntities db;
        private CPCPortalEntities db;

        private SqlConnection con;

        public IEnumerable<CPC_NEWS> GetAllNews()
        {
            //db = new MyCPCPortalEntities();
            //logger.Info("****GetEmployees****" + db.Database.Connection.ServerVersion);
            db = new CPCPortalEntities();
            var news = this.db.CPC_NEWS.OrderBy(x => x.PUBLISH_DATE);
            return news;
        }

        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["CPCPortal"].ToString();
            con = new SqlConnection(constr);
        }

        public List<CPC_NEWS> GetAllNewsBySql()
        {
            Connection();
            List<CPC_NEWS> newsList = new List<CPC_NEWS>();
            SqlCommand cmd = new SqlCommand("SELECT * FROM CPC_NEWS", con);
            con.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                if (!dr[0].Equals(DBNull.Value))
                {
                    CPC_NEWS news = new CPC_NEWS();
                    news.ID = new Guid(dr["ID"].ToString());
                    news.TITLE = dr["TITLE"].ToString();
                    //do something
                    newsList.Add(news);

                }
            }
            con.Close();
            return newsList;
        }
    }
}