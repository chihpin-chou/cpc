﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NLog;
using AutoMapper;
using CPC.DataModel;
using CPC.Web.Repository.Interface;

using System.Data.SqlClient;
using System.Configuration;


namespace CPC.Web.Repository
{
    public class OilStationRepository : IOilStationRepository
    {
        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        //private MyCPCPortalEntities db;
        private CPCPortalEntities db;

        private SqlConnection con;

        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["CPCPortal"].ToString();
            con = new SqlConnection(constr);
        }

        public void CreateOilStation(CPC_OIL_STATION oilstation)
        {
            db = new CPCPortalEntities();
            db.CPC_OIL_STATION.Add(oilstation);
            
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }
            
        }

        public void RemoveAll()
        {
            Connection();
            SqlCommand cmd = new SqlCommand("DELETE FROM CPC_OIL_STATION", con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void CreateOilStationSql(CPC_OIL_STATION oilstation)
        {
            Connection();
            SqlCommand cmd = new SqlCommand("INSERT INTO CPC_OIL_STATION (ID, " +
                "STATION_CODE, " +
                "STATION_NAME, " +                
                "STATION_TYPE, " +
                "COUNTY, " +
                "TOWN, " +
                "ADDRESS, " +
                "TEL, " +
                "SERVICE_CENTER, " +
                
                "[OPEN]," +                
                "GASOLINE92," +
                "GASOLINE95," +
                "GASOLINE98," +
                "ALCOLHOL_GASOLINE," +
                "DIESEL," +
                "MEMBER_CARD," +
                "SELF_SERVICE," +                
                "SELF_SERVICE_DIESEL, " +
                
                "LOCATION," +
                "BUSSINESS_HOUR," +
                
                "INSERTED_BY," +
                "INSERTED," +
                "UPDATED_BY" +
                /*
                "UPDATED " +
                */

                " ) VALUES ( " +
                "@ID, " +
                "@STATION_CODE, " +
                "@STATION_NAME, " +                
                "@STATION_TYPE, " +
                "@COUNTY, " +
                "@TOWN, " +
                "@ADDRESS, " +
                "@TEL, " +
                "@SERVICE_CENTER, " +
                
                "@OPEN," +                
                "@GASOLINE92," +
                "@GASOLINE95," +
                "@GASOLINE98," +
                "@ALCOLHOL_GASOLINE," +
                "@DIESEL," +
                "@MEMBER_CARD," +
                "@SELF_SERVICE," +                
                "@SELF_SERVICE_DIESEL, " +

                "geography::Point(@lat,@lng, 4326)," +
                "@BUSSINESS_HOUR," +
                "@INSERTED_BY," +
                "@INSERTED," +
                "@UPDATED_BY" +
            //    "@UPDATED" +
                
                " )", con);
            con.Open();
            cmd.Parameters.AddWithValue("@ID", oilstation.ID);
            cmd.Parameters.AddWithValue("@STATION_CODE", oilstation.STATION_CODE);
            cmd.Parameters.AddWithValue("@STATION_NAME", oilstation.STATION_NAME);
            
            cmd.Parameters.AddWithValue("@STATION_TYPE", oilstation.STATION_TYPE);
            cmd.Parameters.AddWithValue("@COUNTY", oilstation.COUNTY);
            cmd.Parameters.AddWithValue("@TOWN", oilstation.TOWN);
            cmd.Parameters.AddWithValue("@ADDRESS", oilstation.ADDRESS);
            cmd.Parameters.AddWithValue("@TEL", oilstation.TEL);
            cmd.Parameters.AddWithValue("@SERVICE_CENTER", oilstation.SERVICE_CENTER);
            
            cmd.Parameters.AddWithValue("@OPEN", oilstation.OPEN);
            
            cmd.Parameters.AddWithValue("@GASOLINE92", oilstation.GASOLINE92);
            cmd.Parameters.AddWithValue("@GASOLINE95", oilstation.GASOLINE95);
            cmd.Parameters.AddWithValue("@GASOLINE98", oilstation.GASOLINE98);
            cmd.Parameters.AddWithValue("@ALCOLHOL_GASOLINE", oilstation.ALCOLHOL_GASOLINE);
            cmd.Parameters.AddWithValue("@DIESEL", oilstation.DIESEL);
            cmd.Parameters.AddWithValue("@MEMBER_CARD", oilstation.MEMBER_CARD);
            cmd.Parameters.AddWithValue("@SELF_SERVICE", oilstation.SELF_SERVICE);            
            cmd.Parameters.AddWithValue("@SELF_SERVICE_DIESEL", oilstation.SELF_SERVICE_DIESEL);
            cmd.Parameters.AddWithValue("@lat", oilstation.LOCATION.Latitude);
            cmd.Parameters.AddWithValue("@lng", oilstation.LOCATION.Longitude);
            //cmd.Parameters.Add(new SqlParameter("@LOCATION", oilstation.LOCATION) { UdtTypeName = "Geography" });
            //cmd.Parameters.AddWithValue("@lng", oilstation.LOCATION.Longitude);
            cmd.Parameters.AddWithValue("@BUSSINESS_HOUR", oilstation.BUSSINESS_HOUR);
            cmd.Parameters.AddWithValue("@INSERTED_BY", oilstation.INSERTED_BY);
            cmd.Parameters.AddWithValue("@INSERTED", oilstation.INSERTED);
            cmd.Parameters.AddWithValue("@UPDATED_BY", oilstation.UPDATED_BY);
            //cmd.Parameters.AddWithValue("@UPDATED", oilstation.UPDATED);
            
            cmd.ExecuteNonQuery();
            con.Close();

        }
    }
}