﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CPC.DataModel;

namespace CPC.Web.Repository.Interface
{
    interface IOilStationRepository
    {
        void CreateOilStation(CPC_OIL_STATION oilstation);
        void RemoveAll();
        void CreateOilStationSql(CPC_OIL_STATION oilstation);
    }
}
