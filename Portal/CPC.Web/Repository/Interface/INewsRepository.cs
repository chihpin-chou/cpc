﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPC.DataModel;

namespace CPC.Web.Repository.Interface
{
    interface INewsRepository
    {
        IEnumerable<CPC_NEWS> GetAllNews();
        List<CPC_NEWS> GetAllNewsBySql();
    }
}
