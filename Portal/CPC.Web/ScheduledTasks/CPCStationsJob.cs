﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Quartz;
using System.Net.Http;
using NLog;
using CPC.Integration.Incoming;
using System.Net.Http.Headers;
using System.IO;
using System.Xml.Serialization;
using System.Data;
using System.Xml;
using CPC.DataModel;
using System.Data.Entity.Spatial;
using CPC.Web.Repository;
using CPC.Web.Repository.Interface;

namespace CPC.Web.ScheduledTasks
{
    public class CPCStationsJob : IJob
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private readonly StationInformation _stationInformation;

        public CPCStationsJob()
        {
            HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri("https://vipmember.tmtd.cpc.com.tw/opendata/STNWebService.asmx/getCityStation?City=&Village=&Types=");
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri("https://vipmember.tmtd.cpc.com.tw/opendata/STNWebService.asmx/getCityStation?City=&Village=&Types="),
                Method = HttpMethod.Get,
            };
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));

            _stationInformation = new StationInformation(client, request);
        }
        public CPCStationsJob(StationInformation stationInformation)
        {
            _stationInformation = stationInformation;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Run(() =>
            {
                logger.Info("Execute...");
                if (_stationInformation == null)
                {
                    new CPCStationsJob();
                }                                
                var result = _stationInformation.GetPosts();                
               if (result.IsCompleted)
                {
                    //logger.Info("result:" + result.Result.ToString());                    
                    //新建XML文件類別
                    XmlDocument Xmldoc = new XmlDocument();
                    //從指定的字串載入XML文件
                    //Xmldoc.LoadXml(result.Result.ToString());
                    Xmldoc.Load("D:\\Works\\CPC\\getCityStation.xml");
                    //建立此物件，並輸入透過StringReader讀取Xmldoc中的Xmldoc字串輸出
                    //XmlReader Xmlreader = XmlReader.Create(new System.IO.StringReader(Xmldoc.OuterXml));
                    XmlReader Xmlreader = XmlReader.Create(new System.IO.StringReader(Xmldoc.InnerXml));
                    //建立DataSet
                    DataSet ds = new DataSet();
                    //透過DataSet的ReadXml方法來讀取Xmlreader資料
                    ds.ReadXml(Xmlreader);
                    //建立DataTable並將DataSet中的第0個Table資料給DataTable
                    DataTable _dt = ds.Tables[0];
                    IOilStationRepository oilStationRepository = new OilStationRepository();
                    if (_dt.Rows.Count > 0)
                    {
                        oilStationRepository.RemoveAll();
                    }
                    foreach (DataRow dr in _dt.Rows)
                    {                        
                        string longitude = "";
                        string latitude = "";
                        CPC_OIL_STATION OilStation = new CPC_OIL_STATION();
                        OilStation.ID = System.Guid.NewGuid();
                        // On all tables' columns
                        foreach (DataColumn dc in _dt.Columns)
                        {                            
                            var field1 = dr[dc].ToString();
                            //logger.Info("{0} {1}", dc, field1);
                           
                            if (dc.Caption.Equals("站代號"))
                            {
                                OilStation.STATION_CODE = field1;
                            }
                            else if (dc.Caption.Equals("類別"))
                            {
                                OilStation.STATION_TYPE = field1;
                            }
                            else if (dc.Caption.Equals("站名"))
                            {
                                OilStation.STATION_NAME = field1;
                            }
                            else if (dc.Caption.Equals("縣市"))
                            {
                                OilStation.COUNTY = field1;
                            }
                            else if (dc.Caption.Equals("鄉鎮區"))
                            {
                                OilStation.TOWN = field1;
                            }
                            else if (dc.Caption.Equals("地址"))
                            {
                                OilStation.ADDRESS = field1;
                            }
                            else if (dc.Caption.Equals("電話"))
                            {
                                OilStation.TEL = field1;
                            }
                            else if (dc.Caption.Equals("服務中心"))
                            {
                                OilStation.SERVICE_CENTER = field1;
                            }
                            else if (dc.Caption.Equals("營業中"))
                            {
                                if (field1 == "1")
                                {
                                    OilStation.OPEN = true;
                                }
                                else
                                {
                                    OilStation.OPEN = false;
                                }                                
                            }
                            else if (dc.Caption.Equals("無鉛92"))
                            {
                                OilStation.GASOLINE92 = Boolean.Parse(field1);
                            }
                            else if (dc.Caption.Equals("無鉛95"))
                            {
                                OilStation.GASOLINE95 = Boolean.Parse(field1);
                            }
                            else if (dc.Caption.Equals("無鉛98"))
                            {
                                OilStation.GASOLINE98 = Boolean.Parse(field1);
                            }
                            else if (dc.Caption.Equals("酒精汽油"))
                            {
                                OilStation.ALCOLHOL_GASOLINE = Boolean.Parse(field1);
                            }
                            else if (dc.Caption.Equals("超柴"))
                            {
                                OilStation.DIESEL = Boolean.Parse(field1);
                            }
                            else if (dc.Caption.Equals("會員卡"))
                            {
                                OilStation.MEMBER_CARD = Boolean.Parse(field1);
                            }
                            else if (dc.Caption.Equals("刷卡自助"))
                            {
                                OilStation.SELF_SERVICE = Boolean.Parse(field1);
                            }
                            else if (dc.Caption.Equals("自助柴油站"))
                            {
                                OilStation.SELF_SERVICE_DIESEL = Boolean.Parse(field1);
                            }
                            else if (dc.Caption.Equals("經度"))
                            {
                                longitude = field1;
                            }
                            else if (dc.Caption.Equals("緯度"))
                            {
                                latitude = field1;
                            }
                            else if (dc.Caption.Equals("營業時間"))
                            {
                                OilStation.BUSSINESS_HOUR = field1;
                            }
                            if (longitude != "" && latitude != "")
                            {
                                OilStation.LOCATION = DbGeography.FromText("POINT(" + longitude + " " + latitude + ")");
                                longitude = "";
                                latitude = "";
                            }
                            OilStation.INSERTED_BY = "SYS";
                            OilStation.INSERTED = DateTime.Now;
                            OilStation.UPDATED_BY = "SYS";                            
                        }                        
                        oilStationRepository = new OilStationRepository();
                        oilStationRepository.CreateOilStationSql(OilStation);
                    }
                }
                
            });

        }
    }
}