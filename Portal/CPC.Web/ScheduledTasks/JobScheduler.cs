﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Specialized;

namespace CPC.Web.ScheduledTasks
{
    public class JobScheduler
    {
        public static async void StartAsync()
        {
            var props = new NameValueCollection
            {
                { "quartz.serializer.type", "binary" }
            };
            StdSchedulerFactory schedFact = new StdSchedulerFactory(props);
                    
            IScheduler sched = await schedFact.GetScheduler();
            await sched.Start();

            IJobDetail job = JobBuilder.Create<CPCStationsJob>()
                .WithIdentity("CHTJob", "CHTGroup")
                .Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("CHTTrigger", "CHTGroup")
                .WithSimpleSchedule(x => x.WithIntervalInSeconds(600).RepeatForever())
                .Build();

            await sched.ScheduleJob(job, trigger);
        }


    }
}