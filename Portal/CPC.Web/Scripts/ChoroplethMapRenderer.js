var ChoroplethMapRenderer = Class.create({
	initialize : function() { // 作為建構式
		//讀取tgos map js,因為在 wro 用UriUrilLocator跑 連結會 parse error, 所以改用動態抓
		var loadScript = function(url, type, charset) {
			console.info("load tgos");
			if (type===undefined) type = 'text/javascript';
			if (url) {
				var script = document.querySelector("script[src*='"+url+"']");
				if (!script) {
					var heads = document.getElementsByTagName("head");
					if (heads && heads.length) {
						var head = heads[0];
						if (head) {
							script = document.createElement('script');
							script.setAttribute('src', url);
							script.setAttribute('type', type);
							if (charset) script.setAttribute('charset', charset);
							head.appendChild(script);
						}
					}
				}
				return script;
			}
		};

		loadScript('https://api.tgos.tw/TGOS_API/tgos?ver=2&AppID=x+JLVSx85Lk=&APIKey=in8W74q0ogpcfW/STwicK8D5QwCdddJf05/7nb+OtDh8R99YN3T0LurV4xato3TpL/fOfylvJ9Wv/khZEsXEWxsBmg+GEj4AuokiNXCh14Rei21U5GtJpIkO++Mq3AguFK/ISDEWn4hMzqgrkxNe1Q==', 'text/javascript', 'utf-8');
	},

	/**
	 * setting:{
	 * 	containerId: 繪圖目標的divId,
	 * 	latitudeColumn: 資料來源中做為緯度 的 columnName,
	 *  longitudeColumn: 資料來源中做為經度 的 columnName,
	 *  resourceColumn: 資料來源中作為KML,SHP檔案載入的 columnName,
	 * }
	 */


	render : function(setting, dataset) {
		var contentId = setting.containerId + "content";
		// var contentWrapper = $("#" + mapId);
		var containerWrapper = $("#" + setting.containerId);
		containerWrapper.find("svg").remove();
		containerWrapper.find("div").remove();
		containerWrapper.find("#" + contentId).remove();

		// containerWrapper.empty();
		// containerWrapper.css("padding", "5px");

		var contentWrapper = $("<div></div>");
		contentWrapper.attr("id", contentId);
		contentWrapper.css("width", "100%");
		contentWrapper.css("height", "100%");

		containerWrapper.append(contentWrapper);

		// 繪製地圖
		var latlng1 = new L.LatLng(23.603875, 120.982024);// center
		if(setting.defaultCenter){
			latlng1 = new L.LatLng(setting.defaultCenter.latitude, setting.defaultCenter.longitude);// center
		}
		if(dataset && dataset.length>0 &&setting.latitudeColumn && setting.longitudeColumn){
			latlng1 = new L.LatLng(dataset[0][setting.latitudeColumn], dataset[0][setting.longitudeColumn]);// center
		}

		var defaultZoom = 12;
		if(setting.defaultZoom){
			defaultZoom = setting.defaultZoom;
		}

		if(!setting.valueGroupNumber){
			setting.valueGroupNumber = 6; //有幾組分組, 預設為6
		}

		var zoomControl = true;
		if(setting.disableAllInteract){
			zoomControl = false;
		}
		var map = new L.Map(contentId, {
			center : latlng1,
			zoom : defaultZoom,//,
			zoomControl: zoomControl
			//renderer: L.canvas()
		});

		//leaflet底圖
		this.setBasemap(map);

		//取得值的全距
		if(!setting.domain){
			setting.domain = this.getDomain(setting, dataset);
		}


		//取得每組的MIN跟MAX
		setting.groupIntervals  = this.getGroupIntervals(setting, dataset);

		//如果沒指定顏色, 則用預設顏色集
		if(!setting.colors){
			setting.colors = this.getDefaultGroupColors();
		}

		//加入KML 或SHP 圖層
		this.addKml(setting, dataset, map);

		if(setting.disableAllInteract){
			map.dragging.disable();
			map.touchZoom.disable();
			map.doubleClickZoom.disable();
			map.scrollWheelZoom.disable();
		}

	},

	onEachFeatureBindClosure: function(setting, dataset, map) {
		var self = this;
		return function onEachFeatureBind(feature, layer) {
			if (feature.properties) {
				//var popupHtml = Object.keys(feature.properties)
//		            .map(value =>"<tr><td>" +value + "</td><td>" + feature.properties[value] + "</td></tr>");
//					layer.bindPopup("<table>"+popupHtml+"</table>");

				//.map(function(value){return value + ":" + feature.properties[value]}).join("<br/>");



				layer.on({
					click:	function(e){
						for(var i = 0; i < dataset.length; i++){
							if(feature.properties.name==dataset[i][setting.categoryColumn]){
								var tip = null;
								var pos = e.containerPoint;
								var contentId = setting.containerId + "content";
								var containerWrapper = $("#" + setting.containerId);

								if(setting.popupValueTips){
									tip = setting.popupValueTips;
								} else {
									tip = setting.valueColumn;
								}

								var popupHtml = "<div>縣市：" +feature.properties.name + "</div><div>" + tip + "：" + self.addCommas(dataset[i][setting.valueColumn]) + "</div>";
								$("#mypopup .leaflet-popup-content").html(popupHtml);
								$("#mypopup").show();
								var left = pos.x+containerWrapper.offset().left - ($("#mypopup .leaflet-popup").width()/2);
								var top = pos.y+containerWrapper.offset().top  - $("#mypopup .leaflet-popup").height()-10;
								$("#mypopup").offset({left:left, top:top});

							}
						}
						if(setting.mapFeatureClick){
							setting.mapFeatureClick(feature.properties.name);
						}

					}
				});

				var hasFeatureData = false;

				if(dataset && dataset.length){
					for(var i = 0; i < dataset.length; i++){
						if(feature.properties.name==dataset[i][setting.categoryColumn]){

							layer.setStyle({
								fillColor:self.getColorByValue(setting, dataset[i][setting.valueColumn]),
								color: "#fff",
								weight:1,
								opacity:1,
								fillOpacity:1
							});
							hasFeatureData = true;
							break;
						}
					}
				}

				// 當沒有傳資料時，預設用灰色底的圖塊
				if(!hasFeatureData){
					layer.setStyle({
						color: "#999",
						weight:1,
						opacity:1,
						fillOpacity:0.5
					});
				}


			}
		}

	},
	addKml: function(setting, dataset, map){
		var self = this;
		if(setting.kmlLayerCodes && setting.kmlLayerCodes.length){
			$.each(setting.kmlLayerCodes, function(idx, kmlLayerCode) {
				var kmlUrl = '/ODPResource/public/kml/' + kmlLayerCode;
				var customLayer = L.geoJson(null, {
					// http://leafletjs.com/reference.html#geojson-style
					onEachFeature: self.onEachFeatureBindClosure(setting, dataset, map)
				});
				omnivore.kml(kmlUrl, null, customLayer).addTo(map);
			});
		}
	},
	setBasemap : function(map) {
		$(".leaflet-container").css("background", "white");
	},
	prepareData : function(dataset) {

	},
	getDomain : function(setting, dataset){
		var min = d3.min(dataset, function (d) { return d[setting.valueColumn]; });
		if(min>0){
			min = 0;
		}
		var max = d3.max(dataset, function (d) { return d[setting.valueColumn]; });
		if(max<0){
			max = 0;
		}
		return [min, max];
	},

	/**
	 * 取得每組的MIN跟MAX
	 */
	getGroupIntervals : function(setting, dataset){
		var totalInterval = setting.domain[1] - setting.domain[0]; //全距
		var groupInterval = totalInterval / setting.valueGroupNumber; //組距
		var groupIntervals = [];

		for(var i = 0; i < setting.valueGroupNumber-1; i++){
			groupIntervals[i] = [setting.domain[0] + groupInterval*i, setting.domain[0] + groupInterval*(i+1)];
		}

		//最後一組的組距直接到max, 以避免四捨五入問題
		groupIntervals[setting.valueGroupNumber-1] = [setting.domain[0] + groupInterval*(setting.valueGroupNumber-1), setting.domain[1] + 1];
		return groupIntervals;
	},



	/**
	 * 取得該VALUE落在第幾組
	 * @param setting
	 * @param value
	 * @returns
	 */
	getGroupIndexByValue : function(setting, value){
		var groupIntervals = setting.groupIntervals;
		for(var i = 0; i < groupIntervals.length; i++){
			if(Number(groupIntervals[i][0]) == Number(groupIntervals[i][1]) && Number(groupIntervals[i][0]) == Number(value)){
				// 0, 0的組距特殊狀況, 如空氣品質指標(AQI)-危害站日數 (301-500), 最大值為0, 會導到第1~6組的min,max都是0
				return i;
			} else if(Number(value) >= Number(groupIntervals[i][0]) && Number(value) < Number(groupIntervals[i][1])){
				return i;
			}
		}

		return -1;
	},
	getDefaultGroupColors : function(){
		return ['#63BE7B', '#7DC57C', '#F4E783', '#FFEB84', '#FCAB78', '#F8696B'];
	},
	getColorByValue : function(setting, value){
		var groupIndex = this.getGroupIndexByValue(setting, value);
		return setting.colors[groupIndex];
	},
	addCommas: function(nStr) {
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	}
});
