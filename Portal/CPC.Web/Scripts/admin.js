$( document ).ready(function(){
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        //>=, not <=
        if (scroll >= 200) {
            //clearHeader, not clearheader - caps H
            $("#nav_wrapper").addClass("header-fixed");
            $("#nav_wrapper").animate({top:"0px"});
            $("#header").hide();
        } else {
            $("#nav_wrapper").removeClass("header-fixed");
            $("#header").show();
        }
    }); //missing );

    am4core.ready(function() {
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme3);

        drawLineChart("chartdiv");
        am4core.useTheme(am4themes_myTheme2);
        drawSimpleChart("simplediv");
        drawSimpleChart("carchartdiv");
        drawGaugeChart("gaugediv");
        //drawCarChart("carsvgchartdiv");
        am4core.useTheme(am4themes_myTheme2);
        drawPieChart("carsvgchartdiv");

    }); // end am4core.ready()


    $("#countyList").click(function(){
        $("#countyList").hide();
        $("#stationList").show();
        $("#backToCountyList").show();
    });

    $("#backToCountyList").click(function(){
        $("#countyList").show();
        $("#stationList").hide();
        $("#backToCountyList").hide();
    });

    $("#backToPie").click(function(){
        drawPieChart("carsvgchartdiv");
        $("#backToPie").hide();
    });
});

var am4themes_myTheme = function(target) {
    if (target instanceof am4core.ColorSet) {
        target.list = [
            am4core.color("#1BA68D"),
            am4core.color("#E7DA4F"),
            am4core.color("#E77624"),
            am4core.color("#DF3520"),
            am4core.color("#64297B"),
            am4core.color("#232555")
        ];
    }
};

var am4themes_myTheme2 = function(target) {
    if (target instanceof am4core.ColorSet) {
        target.list = [
            am4core.color("#9DD4B4"),
            am4core.color("#FBE7A3"),
            am4core.color("#679BF8"),
            //am4core.color("#679BF8"),
            am4core.color("#E77624"),
            am4core.color("#64297B"),
            am4core.color("#232555")
        ];
    }
};

var am4themes_myTheme3 = function(target) {
    if (target instanceof am4core.ColorSet) {
        target.list = [

            //am4core.color("#EAE395"),
            am4core.color("#FBE7A3"),
            am4core.color("#679BF8"),
            //am4core.color("#679BF8"),
        ];
    }
};

var drawGaugeChart = function(divId){
    //am4core.useTheme(am4themes_kelly);
    // create chart
    var chart = am4core.create(divId, am4charts.GaugeChart);
    chart.innerRadius = am4core.percent(82);

    /**
     * Normal axis
     */

    var axis = chart.xAxes.push(new am4charts.ValueAxis());
    axis.min = 0;
    axis.max = 100;
    axis.strictMinMax = true;
    axis.renderer.radius = am4core.percent(80);
    axis.renderer.inside = true;
    axis.renderer.line.strokeOpacity = 1;
    axis.renderer.ticks.template.strokeOpacity = 1;
    axis.renderer.ticks.template.length = 10;
    axis.renderer.grid.template.disabled = true;
    axis.renderer.labels.template.radius = 40;
    axis.renderer.labels.template.disabled = true;
    axis.renderer.labels.template.adapter.add("text", function(text) {
        return text + "%";
    })

    /**
     * Axis for ranges
     */

    var colorSet = new am4core.ColorSet();

    var axis2 = chart.xAxes.push(new am4charts.ValueAxis());
    axis2.min = 0;
    axis2.max = 100;
    axis2.renderer.innerRadius = 10
    axis2.strictMinMax = true;
    axis2.renderer.labels.template.disabled = true;
    axis2.renderer.ticks.template.disabled = true;
    axis2.renderer.grid.template.disabled = true;

    var range0 = axis2.axisRanges.create();
    range0.value = 0;
    range0.endValue = 50;
    range0.axisFill.fillOpacity = 1;
    range0.axisFill.fill = colorSet.getIndex(0);

    var range1 = axis2.axisRanges.create();
    range1.value = 50;
    range1.endValue = 100;
    range1.axisFill.fillOpacity = 1;
    range1.axisFill.fill = colorSet.getIndex(2);

    /**
     * Label
     */

    var label = chart.radarContainer.createChild(am4core.Label);
    label.isMeasured = false;
    label.fontSize = 45;
    label.x = am4core.percent(50);
    label.y = am4core.percent(100);
    label.horizontalCenter = "middle";
    label.verticalCenter = "bottom";
    label.text = "30%";


    /**
     * Hand
     */

    var hand = chart.hands.push(new am4charts.ClockHand());
    hand.axis = axis2;
    hand.innerRadius = am4core.percent(20);
    hand.startWidth = 10;
    hand.pin.disabled = true;
    hand.value = 30;

    hand.events.on("propertychanged", function(ev) {
        range0.endValue = ev.target.value;
        range1.value = ev.target.value;
        axis2.invalidate();
    });

    // setInterval(() => {
    //     var value = Math.round(Math.random() * 100);
    //     label.text = value + "%";
    //     var animation = new am4core.Animation(hand, {
    //         property: "value",
    //         to: value
    //     }, 1000, am4core.ease.cubicOut).start();
    // }, 2000);
};

var drawChart = function(divId){
    var chart = am4core.create(divId, am4charts.XYChart);
    chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

    chart.data = [
        {
            country: "USA",
            visits: 23725
        },
        {
            country: "China",
            visits: 1882
        },
        {
            country: "Japan",
            visits: 1809
        },
        {
            country: "Germany",
            visits: 1322
        },
        {
            country: "UK",
            visits: 1122
        },
        {
            country: "France",
            visits: 1114
        },
        {
            country: "India",
            visits: 984
        },
        {
            country: "Spain",
            visits: 711
        },
        {
            country: "Netherlands",
            visits: 665
        },
        {
            country: "Russia",
            visits: 580
        },
        {
            country: "South Korea",
            visits: 443
        },
        {
            country: "Canada",
            visits: 441
        }
    ];

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.dataFields.category = "country";
    categoryAxis.renderer.minGridDistance = 40;
    categoryAxis.fontSize = 11;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.min = 0;
    valueAxis.max = 24000;
    valueAxis.strictMinMax = true;
    valueAxis.renderer.minGridDistance = 30;
// axis break
    var axisBreak = valueAxis.axisBreaks.create();
    axisBreak.startValue = 2100;
    axisBreak.endValue = 22900;
    axisBreak.breakSize = 0.005;

// make break expand on hover
    var hoverState = axisBreak.states.create("hover");
    hoverState.properties.breakSize = 1;
    hoverState.properties.opacity = 0.1;
    hoverState.transitionDuration = 1500;

    axisBreak.defaultState.transitionDuration = 1000;
    /*
    // this is exactly the same, but with events
    axisBreak.events.on("over", function() {
      axisBreak.animate(
        [{ property: "breakSize", to: 1 }, { property: "opacity", to: 0.1 }],
        1500,
        am4core.ease.sinOut
      );
    });
    axisBreak.events.on("out", function() {
      axisBreak.animate(
        [{ property: "breakSize", to: 0.005 }, { property: "opacity", to: 1 }],
        1000,
        am4core.ease.quadOut
      );
    });*/

    var series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.categoryX = "country";
    series.dataFields.valueY = "visits";
    series.columns.template.tooltipText = "{valueY.value}";
    series.columns.template.tooltipY = 0;
    series.columns.template.strokeOpacity = 0;

// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
    series.columns.template.adapter.add("fill", function(fill, target) {
        return chart.colors.getIndex(target.dataItem.index);
    });
};

var drawCarChart = function(divId){
    //am4core.useTheme(am4themes_animated);
    var iconPath = "M352.427,90.24l0.32-0.32L273.28,10.667L250.667,33.28l45.013,45.013c-20.053,7.68-34.347,26.987-34.347,49.707\n" +
        "\t\t\tc0,29.44,23.893,53.333,53.333,53.333c7.573,0,14.827-1.6,21.333-4.48v153.813C336,342.4,326.4,352,314.667,352\n" +
        "\t\t\tc-11.733,0-21.333-9.6-21.333-21.333v-96c0-23.573-19.093-42.667-42.667-42.667h-21.333V42.667C229.333,19.093,210.24,0,186.667,0\n" +
        "\t\t\th-128C35.093,0,16,19.093,16,42.667V384h213.333V224h32v106.667c0,29.44,23.893,53.333,53.333,53.333\n" +
        "\t\t\tc29.44,0,53.333-23.893,53.333-53.333V128C368,113.28,362.027,99.947,352.427,90.24z M186.667,149.333h-128V42.667h128V149.333z\n" +
        "\t\t\t M314.667,149.333c-11.733,0-21.333-9.6-21.333-21.333s9.6-21.333,21.333-21.333c11.733,0,21.333,9.6,21.333,21.333\n" +
        "\t\t\tS326.4,149.333,314.667,149.333z";

    var chart = am4core.create(divId, am4charts.SlicedChart);
    chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect

    chart.data = [{
        "name": "92無鉛",
        "value": 312254
    }, {
        "name": "95無鉛",
        "value": 243135
    }, {
        "name": "98無鉛",
        "value": 181117
    }, {
        "name": "超級柴油",
        "value": 1223133
    }];

    var series = chart.series.push(new am4charts.PictorialStackedSeries());
    series.dataFields.value = "value";
    series.dataFields.category = "name";
    series.alignLabels = true;

    series.maskSprite.path = iconPath;
    series.ticks.template.locationX = 1;
    series.ticks.template.locationY = 0.5;

    series.labelsContainer.width = 100;

    chart.legend = new am4charts.Legend();
    chart.legend.position = "left";
    chart.legend.valign = "bottom";



};


var drawSimpleChart = function(divId){
    //am4core.useTheme(am4themes_material);
    var chart = am4core.create(divId, am4charts.XYChart);
    chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

    chart.data = [
        {
            country: "4月",
            visits: 2134
        },
        {
            country: "5月",
            visits: 1882
        },
        {
            country: "6月",
            visits: 1809
        },
    ];

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.dataFields.category = "country";
    categoryAxis.renderer.minGridDistance = 40;
    categoryAxis.fontSize = 11;
    categoryAxis.renderer.grid.template.strokeOpacity = 1;
    categoryAxis.renderer.grid.template.strokeWidth = 0;


    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.min = 0;
    valueAxis.max = 2500;
    valueAxis.strictMinMax = true;
    valueAxis.renderer.minGridDistance = 30;
    valueAxis.renderer.grid.template.strokeOpacity = 1;
    valueAxis.renderer.grid.template.strokeWidth = 0;

    /*
    // this is exactly the same, but with events
    axisBreak.events.on("over", function() {
      axisBreak.animate(
        [{ property: "breakSize", to: 1 }, { property: "opacity", to: 0.1 }],
        1500,
        am4core.ease.sinOut
      );
    });
    axisBreak.events.on("out", function() {
      axisBreak.animate(
        [{ property: "breakSize", to: 0.005 }, { property: "opacity", to: 1 }],
        1000,
        am4core.ease.quadOut
      );
    });*/

    var series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.categoryX = "country";
    series.dataFields.valueY = "visits";
    series.columns.template.tooltipText = "{valueY.value}";
    series.columns.template.tooltipY = 0;
    series.columns.template.strokeOpacity = 0;

    series.columns.template.events.on("hit", function(ev) {
        if(divId=="carchartdiv"){
            $('#myModal').modal();
        }
    }, this);
// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
    series.columns.template.adapter.add("fill", function(fill, target) {
        return chart.colors.getIndex(target.dataItem.index);
    });
};

var drawLineChart = function(divId){
// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
        var chart = am4core.create(divId, am4charts.XYChart);

// Add data
        chart.data = [{
            "year": "1月",
            "italy": 112345,
            "germany": 112245,
            "uk": 3
        }, {
            "year": "2月",
            "italy": 112445,
            "germany": 112345,
            "uk": 6
        }, {
            "year": "3月",
            "italy": 112545,
            "germany": 112445,
            "uk": 1
        }, {
            "year": "4月",
            "italy": 112235,
            "germany": 112545,
            "uk": 1
        }, {
            "year": "5月",
            "italy": 112145,
            "germany": 112645,
            "uk": 2
        }, {
            "year": "6月",
            "italy": 112645,
            "germany": 112745,
            "uk": 1
        }];

// Create category axis
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "year";

// Create value axis
        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "公升";
        valueAxis.renderer.minLabelPosition = 0.01;

// Create series


        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "germany";
        series2.dataFields.categoryX = "year";
        series2.name = '上年度同期購油量';
        series2.strokeWidth = 3;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = "上年度{categoryX}購油量: {valueY}(公升)";
        series2.legendSettings.valueText = "{valueY}";

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "italy";
        series1.dataFields.categoryX = "year";
        series1.name = "本年度購油量";
        series1.strokeWidth = 3;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = "本年度{categoryX}購油量: {valueY}(公升)";
        series1.legendSettings.valueText = "{valueY}";
        series1.visible  = false;


// Add chart cursor
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.behavior = "zoomY";

// Add legend
        chart.legend = new am4charts.Legend();
};


var drawPieChart = function(divId){
// Themes begin
    am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
    var chart = am4core.create(divId, am4charts.PieChart);

// Add and configure Series
    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "litres";
    pieSeries.dataFields.category = "country";

// Let's cut a hole in our Pie chart the size of 30% the radius
    chart.innerRadius = am4core.percent(30);

// Put a thick white border around each Slice
    pieSeries.slices.template.stroke = am4core.color("#fff");
    pieSeries.slices.template.strokeWidth = 2;
    pieSeries.slices.template.strokeOpacity = 1;
    pieSeries.slices.template
        // change the cursor on hover to make it apparent the object can be interacted with
        .cursorOverStyle = [
        {
            "property": "cursor",
            "value": "pointer"
        }
    ];

    pieSeries.alignLabels = false;
    pieSeries.labels.template.bent = true;
    pieSeries.labels.template.radius = 3;
    pieSeries.labels.template.padding(0,0,0,0);

    pieSeries.ticks.template.disabled = true;

    pieSeries.slices.template.events.on("hit", function(ev) {
        drawPieChart2("carsvgchartdiv");
        $("#backToPie").show();
    }, this);

// Create a base filter effect (as if it's not there) for the hover to return to
    var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
    shadow.opacity = 0;

// Create hover state
    var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

// Slightly shift the shadow and make it more prominent on hover
    var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
    hoverShadow.opacity = 0.7;
    hoverShadow.blur = 5;

// Add a legend
    //chart.legend = new am4charts.Legend();

    chart.data = [{
        "country": "汽油",
        "litres": 807.6
    }, {
        "country": "超級柴油",
        "litres": 139.9}];
};

var drawPieChart2 = function(divId){
// Themes begin
    am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
    var chart = am4core.create(divId, am4charts.PieChart);

// Add and configure Series
    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "litres";
    pieSeries.dataFields.category = "country";

// Let's cut a hole in our Pie chart the size of 30% the radius
    chart.innerRadius = am4core.percent(30);

// Put a thick white border around each Slice
    pieSeries.slices.template.stroke = am4core.color("#fff");
    pieSeries.slices.template.strokeWidth = 2;
    pieSeries.slices.template.strokeOpacity = 1;
    pieSeries.slices.template
        // change the cursor on hover to make it apparent the object can be interacted with
        .cursorOverStyle = [
        {
            "property": "cursor",
            "value": "pointer"
        }
    ];

    pieSeries.alignLabels = false;
    pieSeries.labels.template.bent = true;
    pieSeries.labels.template.radius = 3;
    pieSeries.labels.template.padding(0,0,0,0);

    pieSeries.ticks.template.disabled = true;

// Create a base filter effect (as if it's not there) for the hover to return to
    var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
    shadow.opacity = 0;

// Create hover state
    var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

// Slightly shift the shadow and make it more prominent on hover
    var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
    hoverShadow.opacity = 0.7;
    hoverShadow.blur = 5;

// Add a legend
    //chart.legend = new am4charts.Legend();

    chart.data = [{
        "country": "92無鉛",
        "litres": 501.9
    },{
        "country": "95無鉛",
        "litres": 165.8
    }, {
        "country": "98無鉛",
        "litres": 139.9
    }];
};
