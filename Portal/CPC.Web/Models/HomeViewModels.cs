﻿using System;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CPC.Web.Models
{
    public class HomeViewModels
    {    
        public ICollection<NewsViewModel> NewsModels { get; set; }

        [Required]
        [Display(Name = "帳號")]
        public string Account { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "密碼")]
        public string Password { get; set; }

        [Display(Name = "記住我?")]
        public bool RememberMe { get; set; }

    }
    public class NewsViewModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string PublishDate { get; set; }

    }


}
