﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using CPC.Web.Models;
using NLog;

namespace CPC.Web.Controllers
{
    public class StationController : Controller
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        // GET: /Station/index
        public ActionResult Index()
        {
            logger.Info("GET: /Station/Index");
            return View();
        }
    }
}