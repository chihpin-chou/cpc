﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using CPC.Web.Models;
using NLog;

namespace CPC.Web.Controllers
{
    [Authorize]
    public class StatisticsController : Controller
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        // GET: /Statistics/index
        public ActionResult Index()
        {
            logger.Info("GET: /Statistics/Index");
            return View();
        }
    }
}