﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using CPC.Web.Models;
using NLog;
using System.Web.Security;
using CPC.Web.Repository.Interface;
using CPC.Web.Repository;
using CPC.DataModel;
using System.Collections.Generic;

namespace CPC.Web.Controllers
{
    public class HomeController : Controller
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public HomeController()
        {
        }

        public HomeController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index(string returnUrl)
        {
            INewsRepository newsRepo = new NewsRepository();
            //logger.Info("GET: /Home/Index" + news.GetAllNews().Count());
            CPCUser model = new CPCUser();
            HomeViewModels viewModels = new HomeViewModels();
            //logger.Info("-->" + newsRepo.GetAllNewsBySql().Count());
            viewModels.Account = User.Identity.Name;
            List<NewsViewModel> list = new List<NewsViewModel>();
            foreach (CPC_NEWS news in newsRepo.GetAllNews())
            {
                NewsViewModel newsModel = new NewsViewModel();
                newsModel.Title = news.TITLE;
                newsModel.Content = news.CONTENT;
                TaiwanCalendar taiwanCalendar = new TaiwanCalendar();

                newsModel.PublishDate = string.Format("民國 {0} 年 {1} 月 {2} 日", taiwanCalendar.GetYear(news.PUBLISH_DATE.Value),
                news.PUBLISH_DATE.Value.Month,
                news.PUBLISH_DATE.Value.Day);
                list.Add(newsModel);
            }
            viewModels.NewsModels = list;
            logger.Info("GET: /Home/Index");
            ViewBag.ReturnUrl = returnUrl;
            return View(viewModels);
        }

        public ActionResult About()
        {
            logger.Info("Info");
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        // GET: /Home/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            logger.Info("GET: /Home/Login");
            ViewBag.ReturnUrl = returnUrl;
            return View("Index");
        }

        // POST: /Home/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(CPCUser model, string returnUrl)
        {
            logger.Info("POST: /Home/Login " + ModelState.IsValid);
            logger.Info("POST: /Home/Login returnUrl:" + returnUrl);

            if (!ModelState.IsValid)
            {
                //logger.Info("RedirectToAction: Statistics Index");
                //return RedirectToAction("Index", "Statistics", model);
                return View(model);
            }


            // 這不會計算為帳戶鎖定的登入失敗
            // 若要啟用密碼失敗來觸發帳戶鎖定，請變更為 shouldLockout: true
            //var result = await SignInManager.PasswordSignInAsync(model.Account, model.Password, model.RememberMe, shouldLockout: false);
            var result = SignInStatus.Failure;
            if (IsValid(model))
            {
                FormsAuthentication.RedirectFromLoginPage(model.Account, false);
                if (returnUrl == null)
                {
                    returnUrl = Url.Content("~/Statistics");
                }

                //FormsAuthentication.SetAuthCookie(model.Account, false);
                result = SignInStatus.Success;
            }
            else
            {
                result = SignInStatus.Failure;
            }
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "登入嘗試失試。");
                    return View("Index", model);
                    //return View(model);
            }
        }

        // <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Logout()
        {
            logger.Info("POST: /Home/Logout");
            FormsAuthentication.SignOut();//登出
            return RedirectToAction("Login", "Home");

        }

        private bool IsValid(CPCUser user)
        {
            return (user.Account == "test" && user.Password == "test");
        }


        #region Helper
        // 新增外部登入時用來當做 XSRF 保護
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }


}